<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <!-- Fonts -->
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body class="antialiased">
        <div id="app">
                <navbar :ref="navBar"></navbar>
                <carousel></carousel>
                <div class="page-content">
                    <search-result></search-result>
                </div>
        </div>

        <script src="/js/app.js"></script>
    </body>
</html>
