<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeatherMap
{
    use HasFactory;

    public $container = [];

    public function __construct(array $data = null)
    {

        $this->container['coord'] = isset($data['coord']) ? $data['coord'] : null;
        $this->container['weather'] = isset($data['weather']) ? $data['weather'] : null;
        $this->container['main'] = isset($data['main']) ? $data['main'] : null;
        $this->container['visibility'] = isset($data['visibility']) ? $data['visibility'] : null;
        $this->container['clouds'] = isset($data['clouds']) ? $data['clouds'] : null;
        $this->container['dt'] = isset($data['dt']) ? $data['dt'] : null;
        $this->container['sys'] = isset($data['sys']) ? $data['sys'] : null;

        if($this->container['sys']){
            $this->container['sys']['sunrise'] = date('H:i A',$this->container['sys']['sunrise']);
            $this->container['sys']['sunset'] = date('H:i A',$this->container['sys']['sunset']);
        }
        $this->container['timezone'] = isset($data['timezone']) ? $data['timezone'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['cod'] = isset($data['cod']) ? $data['cod'] : null;
        $this->container['message'] = isset($data['message']) ? $data['message'] : null;
    }

    public function getCoord()
    {
        return $this->container['coord'];
    }

    public function getWeather()
    {
        return $this->container['weather'];
    }

    public function getMain()
    {
        return $this->container['main'];
    }

    public function getVisibility()
    {
        return $this->container['visibility'];
    }

    public function getClouds()
    {
        return $this->container['clouds'];
    }

    public function getDt()
    {
         return $this->container['dt'];
    }

    public function getSys()
    {
        return $this->container['sys'];
    }

    public function getTimezone()
    {
        return $this->container['timezone'];
    }

    public function getName()
    {
        return $this->container['name'];
    }

    public function getCod()
    {
        return $this->container['cod'];
    }

    public function getMessage()
    {
        return $this->container['message'];
    }
}
