<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ObjectSerializer 
{
    use HasFactory;

    public static function deserialize($data,$class)
    {
        $deserialized = ObjectSerializer::jsonToArray($data);

        if(array_key_exists("results", $deserialized))
        {
            $lists = array();

            foreach($deserialized['results'] as $key => $value){
                array_push($lists, new $class($value));
            }

            return $lists;
        }else{
            return new $class(ObjectSerializer::jsonToArray($data));
        }
    }

    public static function jsonToArray($json)
    {
        return json_decode($json,TRUE);
    }
}
