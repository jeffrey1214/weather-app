<?php

namespace App\Models;

use Illuminate\Support\Facades\Http;

class WeatherMapAPI implements APIInterface 
{


    public function getData($api_key, $params)
    {
        return $this->getHttpRequest($api_key, $params);
    }

    public function getHttpRequest($api_key, $params)
    {
        $resource_url = env('OPEN_WEATHER_API_URL');
        $resource_path = '/weather';

        $query = $params;

        $query['appid'] = $api_key;

        $response = Http::get($resource_url.$resource_path, $query);

        $body = $response->body();

        return ObjectSerializer::deserialize($body,'\App\Models\WeatherMap');
   
    }

}
