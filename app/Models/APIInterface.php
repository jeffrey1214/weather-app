<?php

namespace App\Models;


interface APIInterface 
{
    public function getData($api_key,$params);
    public function getHttpRequest($api_key,$params);
}
