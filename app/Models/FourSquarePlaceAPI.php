<?php

namespace App\Models;

use Illuminate\Support\Facades\Http;

class FourSquarePlaceAPI implements APIInterface
{


    public function getData($api_key, $params)
    {
        return $this->getHttpRequest($api_key, $params);
    }

    public function getHttpRequest($api_key, $params)
    {
        $resource_url = env('FOUR_SQUARE_API_URL');
        $resource_path = 'search';

        $query = $params;

        $response = Http::withHeaders([
            "Authorization" => $api_key,
            "Accept" => "application/json"
        ])
        ->get($resource_url.$resource_path, $query);

       $body = $response->body();

        return ObjectSerializer::deserialize($body,'\App\Models\FourSquarePlace');
   
    }

     
}
