<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FourSquarePlace
{
    use HasFactory;

    public $container = [];

    public function __construct(array $data = null)
    {
         $this->container['fsq_id'] = isset($data['fsq_id']) ? $data['fsq_id'] : null;
         $this->container['categories'] = isset($data['categories']) ? $data['categories'] : null;
         $this->container['distance'] = isset($data['distance']) ? $data['distance'] : null;
         $this->container['geocodes'] = isset($data['geocodes']) ? $data['geocodes'] : null;
         $this->container['location'] = isset($data['location']) ? $data['location'] : null;
         $this->container['name'] = isset($data['name']) ? $data['name'] : null;
         $this->container['related_places'] = isset($data['related_places']) ? $data['related_places'] : null;
         $this->container['timezone'] = isset($data['timezone']) ? $data['timezone'] : null;
         $this->container['photos'] = isset($data['photos']) ? $data['photos'] : null;
    }
}
