<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WeatherController extends Controller
{
    //
    public function getForecastToday()
    {
        $params = array();
        $params['q'] = request()->input('city');
        $api_key = env('OPEN_WEATHER_API_KEY');

        $map_api = new \App\Models\WeatherMapAPI();
        $map = $map_api->getData($api_key,$params);
        $data['status'] = "success";
        $data['data']['weather'] = $map->container;
        $data['data']['places'] = array();

        //found city and get nearby places
        if ($map->getCod() == 200){
            $params = array();
            $api_key = env("FOUR_SQUARE_API_KEY");
            $params['ll'] = $map->getCoord()['lat'].",".$map->getCoord()['lon'];
            $params['query'] = $map->getName();
            $params['radius'] = 10000;
            $params['fields'] ="name,fsq_id,geocodes,location,categories,photos";
            $four_square_api = new \App\Models\FourSquarePlaceAPI();
            $places = $four_square_api->getData($api_key,$params);

            foreach($places as $value){
                $data['data']['places'][] = $value->container;
            }

        }else{
            $data['status'] = "failed";
            $data['message'] = $map->getMessage();
        }

        return $data;
    }




}
